# -*- coding: utf-8 -*-
# File Name：     auto_delete_file.py
# Author :        wjh
# date：          2019/5/19
import os

def delete_file(Dir, form):
    '''
        自动删除空文件夹
        以及用户选定的后缀格式文件
    '''
    for i in os.listdir(Dir):
        now_dir = Dir+'\\'+i
        if os.path.isdir(now_dir):
            if os.listdir(now_dir) == []:
                os.removedirs(now_dir)
                print('删除空文件夹:{}'.format(i))
                continue
            next_dir = os.path.join(Dir, i)
            delete_file(next_dir, form)
        else:
            next_dir = os.path.join(Dir, i)
            file_form = os.path.splitext(i)[-1]
            if file_form == form:
                os.remove(next_dir)
                print('删除文件:{}'.format(i))
            else:
                continue

def delete_dir(Dir):
    '''
        删除空文件夹
    '''
    for i in os.listdir(Dir):
        now_dir = Dir+'\\'+i
        if os.path.isdir(now_dir):
            if os.listdir(now_dir) == []:
                os.removedirs(now_dir)
                print('删除空文件夹:{}'.format(i))
                continue
            next_dir = os.path.join(Dir, i)
            delete_dir(next_dir)
        else:
            continue
    

def run(form):
    form = '.' + form
    Dir = os.path.dirname(__file__)
    if form == '.': # 如果用户输入为空，则删除空文件夹
        try:
            delete_dir(Dir)
        except Exception:
            pass
        finally:
            print('已删除当前目录下所有的空文件夹！')
    else: # 如果选定了格式
        try:
            delete_file(Dir, form)
        except Exception:
            pass
        finally:
            print('已删除所有{}格式文件!\n开始为你清理空文件夹...\n'.format(form))
        Dir = os.path.dirname(__file__)
        try:
            delete_dir(Dir)
        except Exception:
            pass
        finally:
            print('已删除当前目录下所有的空文件夹！')

if __name__ == '__main__':
    # 此区域若是觉得麻烦可以删除
    # ***************************************************
    print('''
    #######################################################
        文件清理工具：
            自动清理当前目录下的指定格式文件
            自动删除当前目录下的空文件夹
        请注意！！！！
        一定要将exe文件放在需要删除的目录下再进行操作
    
        数据无价，谨慎操作
        直接输入需要删除的文件格式名
        不用加“.”号
        删除文件会自动清理空文件夹
        直接回车则只清理空文件夹
        文件格式大小写严格区分！
        如果存在未删除的空文件夹，权限不够问题
        Ctrl + C 退出...
    #######################################################
    ''')
    # ******************************************************
    form = input('请输入想要删除的文件格式>>>')
    run(form)
    input()
    