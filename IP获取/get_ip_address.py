# -*- coding: utf-8 -*-
# @Time    : 2018/8/21 19:44
# @Author  : wjh
# @File    : get_ip_address.py
import socket


def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()

    return ip


if __name__ == '__main__':
    a = get_host_ip()
    print(a)