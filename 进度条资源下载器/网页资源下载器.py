import os
import sys
import time
from urllib import request


def Schedule(blocknum, blocksize, totalsize):
    '''
    blocknum:已经下载的数据块
    blocksize:数据块的大小
    totalsize:远程文件的大小
    '''
    speed = blocknum * blocksize / (time.time() - start_time)
    speed_str = 'Speed: %s'%format_size(speed)
    pervent = blocknum * blocksize / totalsize
    percent_str = "%.2f%%" % (pervent * 100)
    n = round(pervent * 50)
    s = ('#' * n).ljust(50, '-')
    f = sys.stdout
    f.write(percent_str.ljust(8, ' ') + '[' + s + ']' + speed_str)
    f.flush()
    f.write('\r')

# 字节bytes转化K\M\G
def format_size(bytes):
    try:
        bytes = float(bytes)
        kb = bytes / 1024
    except:
        print("传入的字节格式不对")
        return "Error"
    if kb >= 1024:
        M = kb / 1024
        if M >= 1024:
            G = M / 1024
            return "%.3fG" % (G)
        else:
            return "%.3fM" % (M)
    else:
        return "%.3fK" % (kb)


if __name__ == '__main__':
    start_time = time.time()
    url = input('URL：')
    filename = input('文件名+格式：')
    path = os.path.dirname(__file__)
    os.chdir(path)
    request.urlretrieve(url, filename, Schedule)
    print('下载完成')




    # url = 'http://vt1.doubanio.com/201811291519/c3dc5b409f78119f92b8583ec3147f24/view/movie/M/402380212.mp4'
    # filename = r'C:\Users\wjh\Desktop\无敌破环王.mp4'