import win32con

import win32clipboard as w

class GetText:
    '''把图像识别的文字，保存到剪切板'''
    @staticmethod
    def getText():
        # 打开剪贴板
        w.OpenClipboard()
        # 获取剪贴板里面的信息
        d = w.GetClipboardData(win32con.CF_UNICODETEXT)
        # 关掉剪贴板
        w.CloseClipboard()
        return d

    @classmethod
    def setText(cls,aString):
        # 打开剪贴板
        w.OpenClipboard()
        # 清空剪贴板
        w.EmptyClipboard()
        # 写入剪贴板
        w.SetClipboardData(win32con.CF_UNICODETEXT,aString)
        # 关闭剪贴板
        w.CloseClipboard()

if __name__ == '__main__':
    GetText.getText()
    GetText.setText('vwooooooorvrvrvrv')
    
    