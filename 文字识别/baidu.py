from aip import AipOcr
import configparser #可读取ini配置文件信息

class BaiDuAPI():
    '''图片文字识别'''

    #初始化函数
    def __init__(self,filePath):
        # 初始化百度文字识别的账号信息
        target = configparser.ConfigParser()
        target.read(filePath)
        app_id = target.get('工单密码','App_ID')
        api_key = target.get('工单密码','API_KEY')
        secret_key = target.get('工单密码','SECRET_KEY')
        # 连接上百度文字识别
        self.client = AipOcr(app_id,api_key,secret_key)


    def picture2Text(self,filePath):
        #读取图片
        images = self.getPicture(filePath)
        texts = self.client.basicGeneral(images)
        allTexts = ''
        for word in texts['words_result']:
            allTexts = allTexts + word.get('words','')
        return allTexts
 

    @staticmethod   #下面的def不用self，就需要静态方法（装饰器）
    def getPicture(
            filePath):
        with open(filePath,'rb') as fp:
            return fp.read()

    

baiduapi = BaiDuAPI('password.ini')
# print(baiduapi.picture2Text('Picture.png'))



