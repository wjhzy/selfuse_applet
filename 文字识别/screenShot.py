# -*- coding: utf-8 -*-
"""
   File Name：     screen_shot
   Author :        bd
   date：          2024/10/9
"""
__author__ = 'bd'

import requests
from PIL import Image, ImageGrab
import pytesseract
from io import BytesIO


# https://brew.idayer.com/
# brew install tesseract
# brew install tesseract-lang

# # 设置tesseract的路径 (Windows用户需要配置路径)
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'  # 请修改为你的安装路径

# # 打开本地图片文件
# image_path = '/Users/bd/Desktop/1721720588954981693619644955953116723.png'  # 替换为你图片的路径
# img = Image.open(image_path)

# # io流
# url = 'https://cdn.aichaoliuapp.cn/hotdog/sale_product/2024-05-27/1716795593690155714812851989138374366.jpg'
# img = Image.open(BytesIO(requests.get(url).content))


def screen_shot():
    # 从剪贴板中抓取图片
    img = ImageGrab.grabclipboard()
    # 检查剪贴板是否包含图片
    if isinstance(img, Image.Image):
        # 使用pytesseract提取图片中的文本
        text = pytesseract.image_to_string(img, lang='chi_sim')  # 'chi_sim'代表使用中文简体字库
        # 打印出识别的文本
        print("-------------")
        print(text)
    else:
        print("剪贴板中没有图片")


def main():
    while True:
        s = input("\n回车开始识别剪切板图片\n")
        screen_shot()


if __name__ == '__main__':
    main()
