import yagmail

from python_learn.SendMail.info import username, password

try:
    #链接邮箱服务器
    yag = yagmail.SMTP( user=username, password=password, host='smtp.163.com', port='465')
    # 邮箱正文
    contents = ['''
    你好，今天中午，我吃饭的时候，我借了你一片姨妈巾，我表示非常的感谢，所以我决定，不还你了，你觉得怎么样？
    
        ''',
                {r'C:\Users\wjh\Pictures\Saved Pictures\2016-12-04 163510.jpg': "hello"}]

    recipients = {
        '1243781831@qq.com': '老婆',
        '1376828025@qq.com': '我自己',
    }

    # 发送邮件
    yag.send(to=recipients,
             subject='感谢信',
             contents=contents)
    print('邮件发送成功')
except Exception as e:
    print('邮件发送失败')
    print(e)