import smtplib
from email.mime.text import MIMEText

from python_learn.SendMail.info import username, password


def Send_Mail(username, password, SendTo, subject, contents):
    # 第三方 SMTP 服务
    mail_host = 'smtp.163.com'  # 设置服务器
    mail_username = username  # 用户名
    mail_auth_password = password  # 授权密码

    sender = username
    # receivers = '1243781831@qq.com'         # 一个收件人
    # receivers = '270114497@qq.com, zhenghaishu@126.com'  # 多个收件人

    # # 这里是邮箱正文内容
    # contents = '你好，这是一个利用python写的邮件发送程序'

    message = MIMEText(contents, 'plain', 'utf-8')
    message['From'] = sender
    message['To'] = SendTo
    message['Subject'] = subject # 邮箱主题

    try:
        smtpObj = smtplib.SMTP(mail_host, 25)  # 生成smtpObj对象，使用非SSL协议端口号25
        # smtpObj = smtplib.SMTP_SSL(mail_host, 465)   # 生成smtpObj对象，使用SSL协议端口号465
        smtpObj.login(mail_username, mail_auth_password)  # 登录邮箱

        if SendTo.count('@') == 1:
            smtpObj.sendmail(sender, SendTo, message.as_string())          # 发送给一人
        # smtpObj.sendmail(sender, receivers.split(','), message.as_string())  # 发送给多人
            print("邮件发送成功")
        else:
            smtpObj.sendmail(sender, SendTo.split(','), message.as_string())
            print('群发成功')
    except smtplib.SMTPException as e:
        print("Error: 无法发送邮件")
        print(e)

if __name__ == '__main__':
    username = username # 发件人账号
    password = password # 发件人账号授权码
    SendTo = '1243781831@qq.com, 1376828025@qq.com' # 收件对象， 群发用英文逗号隔开
    subject = '邮箱主题' # 邮件主题
    contents = '内容' # 输入内容
    Send_Mail(username, password, SendTo, subject, contents) # 发送邮件
