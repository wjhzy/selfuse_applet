# -*-coding:utf-8 -*-
# Author: wjh
# @Time: 2018/7/2 16:29
import requests
import time
import hashlib
import random
import tkinter
import win32con
import win32clipboard as w


from tkinter import messagebox


# 根据用户输入的单词翻译
def translatioin():
    # 获取用户输入的单词
    content = entry1.get()
    if content == '':
        messagebox.showinfo('提示', '请输入需要翻译的单词！')
    else:
        session = requests.session()
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
            'Referer': 'http://fanyi.youdao.com/',
        })
        session.get('http://fanyi.youdao.com/')

        salt = str(int(time.time()*1000)+random.randint(1, 9))
        sign = hashlib.md5(("fanyideskweb" + content +  salt + "p09@Bn{h02_BIEe]$P^nG").encode()).hexdigest()
        ts = salt[:-1]

        url = 'http://fanyi.youdao.com/translate_o'

        data = {
            'i': content,
            'from': 'AUTO',
            'to': 'AUTO',
            'smartresult': 'dict',
            'client': 'fanyideskweb',
            'salt': salt,
            'sign': sign,
            'ts': ts,
            'bv': hashlib.md5(('5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36').encode()).hexdigest(),
            'doctype': 'json',
            'version': 2.1,
            'keyfrom': 'fanyi.web',
            'action': 'FY_BY_REALTIME',
            'typoResult': 'false',
        }
        translatioin = session.post(url, data=data).json()['translateResult'][0][0]['tgt']
        res.set(translatioin)
        time.sleep(0.5)
        # 存入剪切板
        w.OpenClipboard()
        w.EmptyClipboard()
        w.SetClipboardData(win32con.CF_UNICODETEXT, translatioin)
        w.CloseClipboard()

# 创建窗口
root = tkinter.Tk()
# 软件名字
root.title('翻译软件 --by wjh')
# 初始化大小和位置
root.geometry('380x100+500+300')
# 固定窗口大小
root.resizable(0,0)
# 标签空间
lable1 = tkinter.Label(root, text='输入要翻译的文字:', font='微软雅黑')
lable1.grid(row=0, column=0)
lable2 = tkinter.Label(root, text='翻译后的结果:', font='微软雅黑')
lable2.grid(row=1, column=0)
# 变量
res = tkinter.StringVar()
# 输入控件
entry1 = tkinter.Entry(root, font=('微软雅黑', 15))
entry1.grid(row=0, column=1)
entry2 = tkinter.Entry(root, font=('微软雅黑', 15), textvariable=res)
entry2.grid(row=1, column=1)
# 按钮控件
button1 = tkinter.Button(root, text='翻译', width=10, font=('微软雅黑', 10), command=translatioin)
button1.grid(row=2, column=0, sticky='w')
button2 = tkinter.Button(root, text='退出', width=10, font=('微软雅黑', 10), command=root.quit)
button2.grid(row=2, column=1, sticky='e')

# 消息循环，显示窗口
root.mainloop()
