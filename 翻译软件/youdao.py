import json
import requests
import hashlib
import time
import random
import win32con
import win32clipboard as w


def youdao(word):
    session = requests.session()
    session.headers.update({'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36','Referer': 'http://fanyi.youdao.com/',})
    session.get('http://fanyi.youdao.com/')
    salt = str(int(time.time()*1000)+random.randint(1, 9))
    sign = hashlib.md5(("fanyideskweb" + word +  salt + "p09@Bn{h02_BIEe]$P^nG").encode()).hexdigest()
    ts = salt[:-1]
    url = 'http://fanyi.youdao.com/translate_o'
    data = {
        'i': word,
        'from': 'AUTO',
        'to': 'AUTO',
        'smartresult': 'dict',
        'client': 'fanyideskweb',
        'salt': salt,
        'sign': sign,
        'ts': ts,
        'bv': hashlib.md5(('5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36').encode()).hexdigest(),
        'doctype': 'json',
        'version': '2.1',
        'keyfrom': 'fanyi.web',
        'action': 'FY_BY_CLICKBUTTION',
        'typoResult': 'false',
    }
    resposne = session.post(url, data=data)
    info = (json.loads(resposne.text)).get('translateResult')[0][0].get('tgt')
    print(info)
    time.sleep(0.5)
    # 存入剪切板
    w.OpenClipboard()
    w.EmptyClipboard()
    w.SetClipboardData(win32con.CF_UNICODETEXT, info)
    w.CloseClipboard()

if __name__ == '__main__':
    youdao('我不知道该怎那么说出口')
    