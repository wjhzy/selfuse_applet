import os
import requests
import time

from PIL import Image
from io import BytesIO

session = requests.session()

cookie = {
    'cookie': 'Ecp_notFirstLogin=fzW2HL; ecp_uid5=a454870c2e4e0e4d02faf9cf4f1e17df; Ecp_ClientId=8181227163101203030; cnkiUserKey=11320584-1d70-899a-93f1-3a1579f18009; Ecp_lout=1; ASP.NET_SessionId=hdzeh0zew0looqck1olsn0j3; SID=111058; IsLogin=; LID=WEEvREcwSlJHSldRa1FhdXNXaEd1OFkwZW8ydy81TmhKTFdzcnlLWVI5Zz0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!; Ecp_session=1; c_m_LinID=LinID=WEEvREcwSlJHSldRa1FhdXNXaEd1OFkwZW8ydy81TmhKTFdzcnlLWVI5Zz0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!&ot=03/30/2019 15:15:27; c_m_expire=2019-03-30 15:15:27; Ecp_LoginStuts=%7B%22IsAutoLogin%22%3Afalse%2C%22UserName%22%3A%22WH0045%22%2C%22ShowName%22%3A%22%25E6%25B9%2596%25E5%258C%2597%25E5%25B7%25A5%25E7%25A8%258B%25E5%25AD%25A6%25E9%2599%25A2%22%2C%22UserType%22%3A%22bk%22%2C%22r%22%3A%22fzW2HL%22%7D'
}

session.headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36'
}

cookiejar = requests.utils.cookiejar_from_dict(cookie)

session.cookies = cookiejar

session.get('http://kreader.cnki.net/Kreader/CatalogViewPage.aspx?dbCode=cdmd&filename=1018211318.nh&tablename=CMFD201802&compose=&first=1&uid=WEEvREcwSlJHSldRa1FhdXNXaEd1OFkwZW8yeDB4SXhoR3hJU3RFVUJPWT0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!')

if not os.path.exists(r'C:\Users\wjh\Desktop\result'):
    os.mkdir(r'C:\Users\wjh\Desktop\result')
    try:
        for i in range(1, 111):
            resp = session.get('http://kreader.cnki.net/Kreader/OpenFile.ashx?dbCode=cdmd&doc=1018211318.nh&tabelName=CMFD201802&format=png&uid=WEEvREcwSlJHSldRa1FhdXNXaEd1OFkwZW8yeDB4SXhoR3hJU3RFVUJPWT0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!&page={}&mpc=%20110&r=0.38300635469943956'.format(i)).content
            with open(r'C:\Users\wjh\Desktop\result\{}.png'.format(i), 'wb') as f:
                f.write(resp)
                print(f'论文第{i}页下载成功')
                time.sleep(2)
                
    except Exception as e:
        print(f'下载失败,失败原因：{e}')
    else:
        print('下载完成')
else:
    try:
        for i in range(1, 111):
            resp = session.get('http://kreader.cnki.net/Kreader/OpenFile.ashx?dbCode=cdmd&doc=1018211318.nh&tabelName=CMFD201802&format=png&uid=WEEvREcwSlJHSldRa1FhdXNXaEd1OFkwZW8yeDB4SXhoR3hJU3RFVUJPWT0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!&page={}&mpc=%20110&r=0.38300635469943956'.format(i)).content
            with open(r'C:\Users\wjh\Desktop\result\{}.png'.format(i), 'wb') as f:
                f.write(resp)
                print(f'论文第{i}页下载成功')
                time.sleep(2)
                
    except Exception as e:
        print(f'下载失败,失败原因：{e}')
    else:
        print('下载完成')


#url = 'http://kreader.cnki.net/Kreader/OpenFile.ashx?dbCode=cdmd&doc=1016015805.nh&tabelName=CMFD201601&format=json&uid=WEEvREcwSlJHSldRa1Fhb09jT0pkRllaRUlSKzFtRlU2THFScTJTb0JwZz0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!&page=10&mpc=%2055&r=0.5085685749768043&_=1553697306636'
#url = 'http://kreader.cnki.net/Kreader/OpenFile.ashx?dbCode=cdmd&doc=1016015805.nh&tabelName=CMFD201601&format=json&uid=WEEvREcwSlJHSldRa1Fhb09jT0pkRllaRUlSKzFtRlU2THFScTJTb0JwZz0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!&page=10&mpc=%2055&r=0.5085685749768043&_=1553697599776'


# import requests
# import random
# import re

# from PIL import Image
# from io import BytesIO
# from lxml import etree

# class ZhiWangDownLoad():

#     def __init__(self, url):
#         self.session = requests.session()
#         self.session.headers.update({
#             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
#             'Accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
#             'Accept-Encoding': 'gzip, deflate',
#             'Accept-Language': 'zh-CN,zh;q=0.9',
#             'Cache-Control': 'no-cache',
#             'Connection': 'keep-alive',
#         })
#         self.url = url

#     def zhiwang_download(self, doc):
#         url = 'http://kreader.cnki.net/Kreader/OpenFile.ashx?'
#         params = {
#             'dbCode': 'cdmd',
#             'doc': doc,
#             'tabelName': 'CMFD201601',
#             'format': 'png',
#             'uid': 'WEEvREcwSlJHSldRa1Fhb09jT0pkRllaRUlSKzFtRlU2THFScTJTb0JwZz0=$9A4hF_YAuvQ5obgVAqNKPCYcEjKensW4IQMovwHtwkF4VYPoHbKxJw!!',
#             'page': '2',
#             'mpc':  '55',
#             'r': random.random(),
#         }
#         response = requests.get(url, params=params)

#         image = Image.open(BytesIO(response.content))
#         image.format = 'png'
#         image.show()

#     def get_doc(self):
#         response = self.session.get(self.url)
#         html = etree.HTML(response.text)
#         doc = html.xpath('//input[@id="filename"]/@value').first()
#         return doc
#         # doc = re.findall(r'''<script type="text/javascript" language="javascript">
#         # WriteRight('//www.cnki.net/KCMS/detail/frame/datalist.aspx', "cdmd", "cdmd", "{.*?}", "CatalogViewPage.aspx", '//www.cnki.net/KCMS/detail/search.aspx', "", "True");
#         # function Hide() {''', response.text)
#         # print(doc)

# if __name__ == '__main__':
#     zwlw = ZhiWangDownLoad('http://kreader.cnki.net/Kreader/CatalogViewPage.aspx?tablename=CMFD2016&filename=1016015805.nh&dbCode=cdmd')
#     zwlw.get_doc()
    

