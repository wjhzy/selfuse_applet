html = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>论文在线阅读—中国知网</title>
    <link rel="stylesheet" type="text/css" href="//piccache.cnki.net/kdn/kreader/css/GB_min/viewpage.min.css" />
    <link rel="stylesheet" type="text/css" href="//piccache.cnki.net/kdn/kreader/css/GB_min/download.min.css" />
    <link rel="stylesheet" type="text/css" href="//piccache.cnki.net/kdn/kreader/css/GB_min/simple.min.css" />
    <link rel="stylesheet" type="text/css" href="//piccache.cnki.net/kdn/kreader/css/GB_min/tree.min.css" />
    <script type="text/javascript" src="//piccache.cnki.net/kdn/kreader/scripts/min/jquery1.8.3.min.min.js" ></script>
    <script type="text/javascript" src="//piccache.cnki.net/kdn/kreader/Scripts/Lang/GB/min/msg.min.js" ></script>
    <script type="text/javascript" src="//piccache.cnki.net/kdn/kreader/scripts/min/share.min.js" ></script>
    <script type="text/javascript" src="//piccache.cnki.net/kdn/kreader/scripts/min/view.min.js" ></script>
    <script type="text/javascript" src="//piccache.cnki.net/kdn/kreader/scripts/min/request.min.js" ></script>
    <script src="Scripts/layer.js" type="text/javascript"></script>
    <script type="text/javascript" src="//piccache.cnki.net/kdn/KCMS/detail/js/piwikCommon70.js"></script>
</head>
<body>
    <form method="post" action="CatalogViewPage.aspx?tablename=CMFD2016&amp;filename=1016015805.nh&amp;dbCode=cdmd" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMjAxNzE1MDE2NA9kFgJmD2QWAgIBD2QWBGYPFgIeBFRleHQFmQXkuIvovb3vvJo8c3BhbiBzdHlsZT0icGFkZGluZy1yaWdodDo1cHg7Ij48aW1nIHN0eWxlPSJwYWRkaW5nLWJvdHRvbTo1cHg7IiBzcmM9Ii8vcGljY2FjaGUuY25raS5uZXQva2RuL2tyZWFkZXIvaW1hZ2VzL0NBSl9Ob1JpZ2h0LmdpZiIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiAvPjxhIHN0eWxlPSJ6b29tOjEiIHRpdGxlPSJDQUrlhajmlofkuIvovb0iIGNsYXNzPSJ0aXRsZUNsYXNzIiAgaHJlZj0iLy9rZG9jLmNua2kubmV0L2tkb2MvZG93bmxvYWQuYXNweD9maWxlbmFtZT12VVVVdklHYlpsRWJYVm5SMHNtWll4VWVQaG1hQnhFZHJjMGNMUmtRbzVtUm5wR08wVm5WQ0prWk90a041ZG5Ub1ZtUnJabFEzSmxURkZuY3N4RWMwTVZWekptU3JGbE1yWkViU2xHY3JrRVNOaDBMekZEVmxaa1NZVjBWek4zUnRaMlJHdDJSQmwxUjVaRmNLQkZORHBWYnRGbVExd1dPJnRhYmxlbmFtZT1DTUZEMjAxNjAxIiB0YXJnZXQ9Il9ibGFuayI+Q0FK5YWo5paH5LiL6L29PC9hPiZuYnNwOyZuYnNwOyZuYnNwOyZuYnNwOzxzcGFuPjxhIHN0eWxlPSJ6b29tOjEiIHRpdGxlPSJDQUrmtY/op4jlmajkuIvovb0iIGNsYXNzPSJ0aXRsZUNsYXNzIiAgaHJlZj0iaHR0cDovL2NhanZpZXdlci5jbmtpLm5ldCIgdGFyZ2V0PSJfYmxhbmsiPkNBSua1j+iniOWZqOS4i+i9vSA8L2E+PC9zcGFuPmQCAQ8WAh4HVmlzaWJsZWgWBAIBDw8WAh8BaGRkAgMPFgIfAWgWBGYPDxYCHwAFA+W5tGRkAgEPDxYCHwAFA+acn2RkZHsuKdZs6Ew9yGaJE1VWTYfFk4oJ+Am8JYJuBjuI+Dao" />
</div>

<div class="aspNetHidden">

        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAUz0sMhwLTkqaQBSnGxeJ3tqLK29cJY28WVFLzxLLFvV74Ji6Jibz7zjW1wbnx9I0dvDhWv5kO3yRhNnZd9uJ/Gekz4gSBHduAWa0N1BFBzujCLfXydunUaRug41TJ0e1ARbxMCQWwUzaizdoUet1Bx" />
</div>
        <div id="HeaderDiv">



<script type="text/javascript" src="//login.cnki.net/TopLogin/api/loginapi/get?type=top&returnurl=&style=2&iswithiplogin=false"></script>


<style>
    /* 弹窗 */
    .window-pay {
        width: 640px;
        font-size: 18px;
        color: #333;
        background: white;
        font-family: "Microsoft Yahei";
    }

    .window-pay-title {
        background-color: #f1f1f1;
        padding-left: 35px;
        line-height: 47px;
        position: relative;
        border-radius: 8px 8px 0 0;
        border-bottom: 1px solid #dcdcdc;
    }

    .window-pay-close {
        position: absolute;
        right: 10px;
        top: 0;
        line-height: 42px;
        color: #333;
        font-size: 32px;
    }

    .window-pay-body {
        padding: 20px 35px 30px 35px;
    }

    .window-pay-name {
        padding-bottom: 17px;
        border-bottom: #f1f1f1 1px solid;
        padding-left: 60px;
        line-height: 34px;
        padding-top: 10px;
        font-weight: bold;
        background: url(//piccache.cnki.net/kdn/kreader/images/RechargeIcon.png) left 5px no-repeat;
    }

    .window-pay-info {
        margin-top: 21px;
        font-size: 14px;
        padding-left: 60px;
        margin-bottom: 30px;
    }

    .window-pay-item {
        margin-bottom: 9px;
    }

    .window-pay-btn {
        width: 230px;
        height: 46px;
        line-height: 46px;
        color: white;
        text-align: center;
        border-radius: 5px;
        margin: 0 auto;
        font-weight: bold;
        box-shadow: 2px 2px 5px 1px rgba(180,180,180,0.91);
        display: block;
    }

    .window-pay-btn-submit {
        background-color: #d81718;
    }

        .window-pay-btn-submit:hover {
            background-color: #ce0101;
        }

    .window-pay-btn-read {
        background-color: #2aba06;
    }

        .window-pay-btn-read:hover {
            background-color: #2da80d;
        }

    .window-pay-info-submit {
        margin-top: 20px;
        font-size: 12px;
        color: #c11920;
        margin-bottom: 17px;
        text-align: center;
    }

    .window-pay-info-read {
        margin-top: 20px;
        font-size: 12px;
        color: #333;
        margin-bottom: 17px;
        text-align: center;
    }

    .window-pay-footer {
        border-top: 1px solid #f1f1f1;
        padding-top: 30px;
        font-size: 12px;
        color: #666;
        line-height: 24px;
    }
</style>




<div class="Logo" style="top: 6px;">
    <a href="//www.cnki.net">
        <img alt="中国知网" src='//piccache.cnki.net/kdn/kreader/images/logo.gif'><img src='//piccache.cnki.net/kdn/kreader/images/home.gif' class="logohome"></a>
</div>
<div class="line-box" style="height: 60px;">
    <h1 style="line-height: 60px;">在线阅读</h1>
</div>


<div id="winPay" class="window-pay" style="display:none;">
    <div class="window-pay-body">
        <div id="payTitle" class="window-pay-name">
        </div>
        <div class="window-pay-content">
            <div class="window-pay-info">
                <div id="payFileType" class="window-pay-item"></div>
                <div id="payPrice" class="window-pay-item"></div>
                <div id="payPages" class="window-pay-item"></div>
                <div id="payTotalPrice" class="window-pay-item"></div>
            </div>
            <a id="btn001" href="javascript:void(0)" style="color:white;text-decoration-line:none" class="window-pay-btn window-pay-btn-read">立 即 阅 读
            </a>
            <div  id="btn002"  class="window-pay-info-read">
                点击按钮，系统扣除账户费用后开始在线阅读全文
            </div>

              <a  id="btn101" href="http://my.cnki.net/CNKIRecharging/czzx.html" target="_blank"  style="color:white;text-decoration-line:none"  class="window-pay-btn window-pay-btn-submit">
                    立 即 充 值
                </a>
                <div id="btn102" class="window-pay-info-submit">
                    您的账户余额不足，请充值后阅读或下载全文。
                </div>
        </div>
        <div class="window-pay-footer">
            温馨提示: 7日内重复阅读、下载同一篇文献，系统只收取一次费用。
        </div>
    </div>
</div>

<script type="text/ecmascript">
    $(".ecp_top-nav").css({
        "border-top": "none"
    });

    FlushLogin();


    function LoginSucess(data) {
        document.getElementById('readPage').contentWindow.location.reload(true);
    }

    function Ecp_LogoutOptr(data) {

        $.get("logout.aspx?q=1", function (result) {

        window.location.reload();

        });

    }


    //提示充值
    function ShowRecharge(title, fileType, price, pages, totalPrice,tag) {
        $("#payTitle").html(title);
        $("#payFileType").html(fileType);
        $("#payPrice").html(price);
        $("#payPages").html(pages);
        $("#payTotalPrice").html(totalPrice);

        if (tag == 1) {
            $("#btn001").show();
            $("#btn002").show();
            $("#btn101").hide();
            $("#btn102").hide();
        }
        else {
            $("#btn001").hide();
            $("#btn002").hide();
            $("#btn101").show();
            $("#btn102").show();
        }

        var idx = window.layer({
            type: 1,
            title: ['支付信息', true],
            area: ['640px', 'auto'],
            page: { dom: '#winPay' }
        });
        $("#btn001").unbind("click").bind("click", function () {
            $('#readPage')[0].contentWindow.Deduction();
            layer.close(idx);
        });
    }
    //登录窗口
    function ShowLoginLayer(optionStr) {

        $("#Ecp_top_login").click();
    }

    //强制修改第三方登录地址
    function Redirect3LoginUrl() {

        if (typeof (window.location.href) != "undefined") {
            url = window.location.href;
        } else {
            url = "//www.cnki.net";
        }
        url = delQueStr(url, "uid");

        $("#Ecp_HyperLinkQQ").attr("href", "http://my.cnki.net/ThirdLogin/ThirdLogin.aspx?to=qq&RedirectUrl=" + encodeURIComponent(url));
        $("#Ecp_HyperLinkWeixin").attr("href", "http://my.cnki.net/ThirdLogin/ThirdLogin.aspx?to=weixin&RedirectUrl=" + encodeURIComponent(url));
        $("#Ecp_HyperLink163").attr("href", "http://my.cnki.net/ThirdLogin/ThirdLogin.aspx?to=163&RedirectUrl=" + encodeURIComponent(url));
        $("#Ecp_HyperLinkSina").attr("href", "http://my.cnki.net/ThirdLogin/ThirdLogin.aspx?to=sina&RedirectUrl=" + encodeURIComponent(url));


        $(".register a.ljzc").attr("href", "http://my.cnki.net/elibregister/commonRegister.aspx");

    }

    function delQueStr(url, ref) //删除参数值
    {
        var str = "";

        if (url.indexOf('?') != -1)
            str = url.substr(url.indexOf('?') + 1);
        else
            return url;
        var arr = "";
        var returnurl = "";
        var setparam = "";
        if (str.indexOf('&') != -1) {
            arr = str.split('&');
            for (i in arr) {
                if (arr[i].split('=')[0] != ref) {
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                }
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
        }
        else {
            arr = str.split('=');
            if (arr[0] == ref)
                return url.substr(0, url.indexOf('?'));
            else
                return url;
        }
    }

    //点击登录事件
    function myEcp_ShowLoginLayer2() {
        $('#Ecp_errorMsg').text("").hide();
        $(".login-title").css("margin-bottom", "24px");
        $(".shadow").show();
        $(".modal").show();
        $(".login-modal").show();
        $('#Ecp_top_login_layer').animate({ "top": "50%", "left": "50%", "margin-top": "-173px" }, 800);
        $('#Ecp_top_login_oversea').animate({ "top": "50%", "left": "50%", "margin-top": "-173px" }, 800);
        $('#Ecp_top_login_oversea').hide();
        //$('#Ecp_TextBoxUserName').focus();
    }
    //点击关闭事件
    function myEcp_ShowLoginLayer3() {
        $('#Ecp_top_login_layer').animate({ "top": "-345px", "margin-top": 0 }, 800,
                            function () { $(".modal").hide(); $(".shadow").hide(); });
    }
    //
    function OtherChange() {
        $("#Ecp_top_login").unbind("click").bind("click", function () { myEcp_ShowLoginLayer2(); Redirect3LoginUrl(); });
        $("#Ecp_top_login_closeLayer").unbind("click").bind("click", function () { myEcp_ShowLoginLayer3(); });
        if ($("#Ecp_loginShowName").text() != undefined && $("#Ecp_loginShowName").text() != '') {
            $("#Ecp_header_Register").hide();
        }
        else { $("#Ecp_header_Register").show(); }
    }
    OtherChange();

</script>

        </div>
        <div id="mainContent" style="position: relative; width: 1000px; margin: auto; padding: auto;">
            <img id="loading" src='//piccache.cnki.net/kdn/kreader/images/loading.gif' title="正在加载..." style="display: none; position: absolute; top: 10px; left: 500px;" />
            <div class="head_cdmd">
                <div id="head_name_cdmd">
                    <h3>
                        <a class="titleClass" href="//kns.cnki.net/KCMS/detail/detail.aspx?filename=1016015805.nh&dbname=CMFD201601&dbcode=cdmd&uid=&v=MTkxNzNIYmtxV0EwRnJDVVJMT2ZaT1JuRnkva1VyclBWRjI2R0xPNUc5bk1xcEViUElSK2ZuczR5UllhbXoxMVA=" title='广州番禺职业技术学院仪器设备管理系统的设计与实现' target="_blank" >广州番禺职业技
术学院仪器设备管理系统的设计与实现</a></h3>
                </div>
                <div style="float: right" id="downLoadFile">
                    下载：<span style="padding-right:5px;"><img style="padding-bottom:5px;" src="//piccache.cnki.net/kdn/kreader/images/CAJ_NoRight.gif" width="16" height="16" /><a style="zoom:1" title="CAJ全文下载" class="titleClass"  href="//kdoc.cnki.net/kdoc/download.aspx?filename=vUUUvIGbZlEbXVnR0smZYxUePhmaBxEdrc0cLRkQo5mRnpGO0VnVCJkZOtkN5dnToVmRrZlQ3JlTFFncsxEc0MVVzJmSrFlMrZEbSlGcrkESNh0LzFDVlZkSYV0VzN3RtZ2RGt2RBl1R5ZFcKBFNDpVbtFmQ1wWO&tablename=CMFD201601" target="_blank">CAJ全文下
载</a>&nbsp;&nbsp;&nbsp;&nbsp;<span><a style="zoom:1" title="CAJ浏览器下载" class="titleClass"  href="http://cajviewer.cnki.net" target="_blank">CAJ浏览器下载 </a></span>
                </div>
            </div>
            <!--leftside-->
            <div id="position">
            </div>
            <div id="leftside">
                <div>

                    <iframe id="treeView" src="buildTree.aspx?dbCode=cdmd&FileName=1016015805.nh&TableName=CMFD201601&sourceCode=GJLIN&date=&year=2015&period=06&fileNameList=&compose=&subscribe=&titleName=&columnCode=&previousType=_&uid="
                        frameborder="0" width="100%" marginheight="0" marginwidth="0" scrolling="auto"></iframe>
                </div>
            </div>
            <!--rightside-->
            <div id="rightside">
                <div id="LeftBar" class="leftbar_syl" title="边栏">
                    <span id="LeftBarSpan" class="leftbar_span leftbar_open"></span>
                </div>
                <iframe id="readPage" name="readPage" src="ReadPage.aspx?dbCode=cdmd&filename=1016015805.nh&tablename=CMFD201601&uid=&cpn=1&columnCode=&compose=&fileNameList="
                    frameborder="0" width="100%" marginheight="0" marginwidth="0" scrolling="no"></iframe>
            </div>
            <input type="hidden" name="hidcdmd" id="hidcdmd" value="[D]" />
            <input type="hidden" name="hidTableName" id="hidTableName" />
            <input type="hidden" name="hidFileName" id="hidFileName" />
            <input type="hidden" name="hiddbCode" id="hiddbCode" />
            <div class="botlikelist">
                <h4>
                    <b>基本信息</b></h4>
                <ul id="baseInfo">
                    <li id="key" style="word-wrap: break-word; word-break: normal;"><b>&nbsp;关键词：</b> &nbsp;仪器设备;管理系统;B/S结构;ASP;SQL Server 2005数据库;ADO;</li>
                    <li id="num"><b>&nbsp;下载频次：</b>39
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>被引频次：</b>0</li>
                </ul>
            </div>
            <div class="botlikelist" style="display: none">
                <h4>
                    <b>参考文献</b><span id="referListmore" name="referListmore" class="morestyle"><a href='//www.cnki.net/KCMS/detail/search.aspx?dbcode=cdmd&sfield=ref&skey=%e5%b9%bf%e5%b7%9e%e7%95%aa%e7%a6%ba%e8%81%8c%e4%b8%9a%e6%8a%80%e6%9c%af%e5%ad%a6%e9%99%a2%e4%bb%aa%e5%99%a8%e8%ae%be%e5%a4%87%e7%ae%a1%e7%90%86%e7%b3%bb%e7%bb%9f%e7%9a%84%e8%ae%be%e8%ae%a1%e4%b8%8e%e5%ae%9e%e7%8e%b0&code=1016015805.nh&uid='
                        target='_blank'>更多&gt;&gt;</a></span></h4>
                <ul id="referList">
                </ul>
            </div>
            <div class="botlikelist" style="display: none">
                <h4>
                    相似文献</h4>
                <ul id="listSimilar">
                </ul>
            </div>
            <div class="botlikelist" style="display: none">
                <h4>
                    <b>引证文献</b><span id="citedcontentmore" name="citedcontentmore" class="morestyle"><a
                        href='//www.cnki.net/KCMS/detail/search.aspx?dbcode=cdmd&sfield=cite&skey=%e5%b9%bf%e5%b7%9e%e7%95%aa%e7%a6%ba%e8%81%8c%e4%b8%9a%e6%8a%80%e6%9c%af%e5%ad%a6%e9%99%a2%e4%bb%aa%e5%99%a8%e8%ae%be%e5%a4%87%e7%ae%a1%e7%90%86%e7%b3%bb%e7%bb%9f%e7%9a%84%e8%ae%be%e8%ae%a1%e4%b8%8e%e5%ae%9e%e7%8e%b0&code=1016015805.nh&uid='
                        target='_blank'>更多&gt;&gt;</a></span></h4>
                <ul id="citedcontent">
                </ul>
            </div>
        </div>

        <div id="FooterDiv">
            <div class="botNav clearfix">
        <a target="_blank" href="//cnki.net/gycnki/gycnki.htm">关于我们</a>
    <a target="_blank" href="//www.cnki.net/other/gonggao/gongsirongyu.html">CNKI荣誉</a>
        <a target="_blank" href="//www.cnki.net/other/gonggao/bqsm.htm">版权公告</a>
        <a target="_blank" href="//service.cnki.net/">客服中心</a>
        <a target="_blank" href="//help.cnki.net/">在线咨询</a>
    <a href="http://kbs.cnki.net/forums/10873/ShowForum.aspx" target="_blank">用户交流</a>
        <a target="_blank" href="//help.cnki.net/Live800_1_2.aspx?GroupID=503&PageID=101008&Name=&CookieName=TTKN_OnLine_800">用户建议</a>
</div>
<div id="bottom">
<div class="botbox">
<div class="leftinside">
<ul>
<li style="width:120px;"><b>读者服务</b>
<p><a target="_blank" href="http://vipcard.cnki.net/ec/skwd/skwd.htm">购买知网卡</a><br />
<a target="_blank" href="http://my.cnki.net/CNKIRecharging/czzx.html">充值中心</a><br />
<a target="_blank" href="http://my.cnki.net/">我的CNKI</a><br />
<a target="_blank" href="//service.cnki.net/helpcenter/Html/index.html?randomNum=265">帮助中心</a>
</p>
</li>
<li style="width:175px;"><b>CNKI常用软件下载</b>
<p><a target="_blank" href="http://cajviewer.cnki.net">CAJViewer浏览器</a><br />
<a target="_blank" href="//www.cnki.net/software/xzydq.htm#gerenshuzi">桌面版个人数字图书馆软件</a><br />
<a target="_blank" href="//www.cnki.net/software/xzydq.htm#CNKIe-Learning">CNKI数字化学习平台</a><br />
<a target="_blank" href="http://mall.cnki.net/Reference/Soft/CNKI工具书.rar">工具书桌面检索软件</a>
</p>
</li>
<li style="width:135px;"><b>特色服务</b>
<p><a target="_blank" href="http://wap.cnki.net/">手机知网</a><br />
<a target="_blank" href="http://wuxizazhi.cnki.net/">杂志订阅</a><br />
<a target="_blank" href="http://mall.cnki.net/">数字出版物订阅</a><br />
<a target="_blank" href="http://ad.cnki.net/index.aspx">广告服务</a>
</p>
</li>
<li style="width:145px;"><b>客服咨询</b>
<p>订卡热线：400-819-9993<br />
服务热线：400-810-9888<br />
在线咨询：<a target="_blank" href="//help.cnki.net">help.cnki.net</a><br />
邮件咨询：<a target="_blank" href="mailto:help@cnki.net">help@cnki.net</a>
客服微博：<a target="_blank" href="http://e.weibo.com/u/3059308095"><img style="width: 16px; height: 16px;vertical-align:middle;" title="客服新浪微博" src="/kreader/images/logo_xl.gif"/></a><a target="_blank" href="http://t.qq.com/CNKI_kefu" style="margin-left:5px;"><img style="width: 16px; height: 16px;vertical-align:middle;" title="客服腾讯微博" src="/kreader/images/logo_tx.gif"/></a>
</p>
</li>
</ul>
</div>
<div class="rigfooter">
  <img src="/Kreader/images/footlogo.png" width="230" height="28" alt="" /><br />
    京ICP证040431号 <a  target="_blank" href="//www.cnki.net/images/006.jpg" target="_blank">互联网出版许可证 新出网证(京)字008号</a>
<br />
 <a target="_blank" href="http://www.hd315.gov.cn/beian/view.asp?bianhao=010202005012100021">经营性网站备案信息</a> 京公网安备11010802020460号<br />
©1998-2018中国知网(CNKI)<br />《中国学术期刊(光盘版)》电子杂志社有限公司<br />
KDN平台基础技术由KBASE 10.0提供.
</div>
<div class="clear"></div>
</div>
</div>

<script src="//search.cnki.net/KRS//Scripts/Recommend.js" type="text/javascript"></script>
        </div>
        <input type="hidden" id="fileNameList" name="fileNameList" value="" />
        <input type="hidden" id="filename" name="filename" value="1016015805.nh" />
        <input type="hidden" id="shareTitle" name="shareTitle" value="广州番禺职业技术学院仪器设备管理系统的设计与实现. 曾庆帮 中国优秀硕士学位论文全文数据库.2015-06 --文献出自中国知网" />
    </form>

    <form id="SubScrFrom" method="post" action="" target="_blank">
        <input type="hidden" id="U" name="U" value="" />
        <input type="hidden" id="T" name="T" value="4" />
        <input type="hidden" id="C" name="C" value="" />
        <input type="hidden" id="S" name="S" value="" />
    </form>
    <div id="hideRecharge" style="display: none; background-color: #fff; height: 100px;">
        <p style="padding: 10px; text-indent: 2em; line-height: 23px; font-weight: bold">
            您的余额不足，如果继续浏览请充值！
        </p>
        <div>
            <span class="xubox_botton1"><a id="rechBtnOk" class="xubox_botton2_0" target="_blank"
                href="//my.cnki.net/CNKIRecharging/czzx.html">马上充值</a> <a id="rechBtnCancel" class="xubox_botton3_0"
                    href="javascript:;">一会再说</a> </span>
        </div>
    </div>
</body>
<script type="text/javascript" language="javascript">
    WriteRight('//www.cnki.net/KCMS/detail/frame/datalist.aspx', "cdmd", "cdmd", "1016015805.nh", "CatalogViewPage.aspx", '//www.cnki.net/KCMS/detail/search.aspx', "", "True");
    function Hide() {
        if ('仪器设备;管理系统;B/S结构;ASP;SQL Server 2005数据库;ADO;'.length == 0) {
            $("#key").hide();
        }
    }
    Hide();

    var isShowShare = '0';
    if (isShowShare == '1') {
        GetShareWeibosRight();
    }
</script>
</html>'''
